from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template import loader
import pandas as pd
import quickstart
import retrieve
import smtplib
import ssl
from email.message import EmailMessage
from openpyxl import load_workbook
from django.contrib import messages


def detail(request):
    page = loader.get_template('creds.html')
    return HttpResponse(page.render(request=request))


def process(request):
    quickstart.main()
    print(retrieve.retrieve_msg())
    if not retrieve.retrieve_msg():
        exit()
    else:
        if request.method == 'POST':
            mail = request.POST['email']
            password = request.POST['password']
            serve_smtp = request.POST['smtp_server']
            file = pd.read_excel('adrmails.xlsx', sheet_name='IT')
            contacts = pd.DataFrame(file)
            contact = contacts['Username']
            for ids in retrieve.retrieve_msg():
                msg_str = retrieve.get_message(msg_id=ids)
                print(msg_str)
                for i in range(0, contact.size):
                    check = contact[i].casefold() in str(msg_str).casefold()
                    if str(check) == 'True':
                        msg = EmailMessage()
                        msg.set_content(str(msg_str))
                        msg['Subject'] = 'Forward'
                        msg['From'] = mail
                        msg['To'] = str(contacts['Email'][1])
                        context = ssl.create_default_context()
                        with smtplib.SMTP_SSL(serve_smtp, 465, context=context) as server:
                            server.login(mail, password)
                            server.sendmail(msg['From'], msg['To'], msg.as_string())
                            server.quit()
                            print("Le message a bien été envoyé a ", msg['To'], )
                    print('\n')
        return redirect('https://mail.google.com/')


def add_views(request):
    page = loader.get_template('add_account.html')
    return HttpResponse(page.render(request=request))


def add_account(request):
    global sent
    if request.method == 'POST':
        mail = request.POST['email']
        name = request.POST['name']
        dep = request.POST['dep']
        if dep == 'IT':
            book = load_workbook('adrmails.xlsx')
            writer = pd.ExcelWriter('adrmails.xlsx', engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
            dat1 = pd.read_excel('adrmails.xlsx', sheet_name='IT').drop_duplicates().reset_index(drop=True)
            dat3 = pd.DataFrame({'Username': [name], 'Email': [mail], 'Departement': [dep]})
            dat3.to_excel(writer, sheet_name="IT", header=False, startrow=len(dat1)+1, index=False)
            writer.save()
            sent = 1
        elif dep == 'COM':
            book = load_workbook('adrmails.xlsx')
            writer = pd.ExcelWriter('adrmails.xlsx', engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
            dat1 = pd.read_excel('adrmails.xlsx', sheet_name='COM').drop_duplicates().reset_index(drop=True)
            dat3 = pd.DataFrame({'Username': [name], 'Email': [mail], 'Departement': [dep]})
            dat3.to_excel(writer, sheet_name="COM", header=False, startrow=len(dat1) + 1, index=False)
            writer.save()
            sent = 1
        elif dep == 'HR':
            book = load_workbook('adrmails.xlsx')
            writer = pd.ExcelWriter('adrmails.xlsx', engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
            dat1 = pd.read_excel('adrmails.xlsx', sheet_name='HR').drop_duplicates().reset_index(drop=True)
            dat3 = pd.DataFrame({'Username': [name], 'Email': [mail], 'Departement': [dep]})
            dat3.to_excel(writer, sheet_name="HR", header=False, startrow=len(dat1) + 1, index=False)
            writer.save()
            sent = 1
        elif dep != ['IT', 'COM', 'HR']:
            sent = 0

    if sent == 1:
        messages.success(request, 'The new account has been added')
    else:
        messages.warning(request, 'An error occurred. Retry please')

    return redirect('add_views')
