from django.apps import AppConfig


class MaildroidConfig(AppConfig):
    name = 'MailDroid'
