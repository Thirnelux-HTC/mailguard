from __future__ import print_function

import base64
import email
import os.path
import pickle
import tkinter
import pandas as pd
from datetime import datetime, timedelta
from logging import error
from tkinter import *
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient import errors
from googleapiclient.discovery import build

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

with open("last_login.txt", 'w', encoding='utf-8') as fileobject:
    fileobject.write(datetime.strftime(datetime.now(), '%Y-%m-%d'))
    fileobject.close()

with open("last_login.txt", 'r', encoding='utf-8') as fileobject:
    last_in = fileobject.read()


def get_service():
    """Shows basic usage of the Gmail API.
    Lists the user's Gmail labels.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)
    return service


def retrieve_msg(service=get_service(), user_id='me', search_string='in:inbox newer:{}'.format(last_in)):
    try:
        search_id = service.users().messages().list(userId=user_id, q=search_string).execute()
        number_msg = search_id['resultSizeEstimate']
        final_list = []
        if number_msg <= 0:
            print("No new messages since yesterday")
            return final_list
        else:
            messages_id = search_id['messages']
            for ids in messages_id:
                final_list.append(ids['id'])
            return final_list
    except(errors.HttpError, error):
        print("An error occurred !!!")


def get_message(msg_id, service=get_service(), user_id='me'):
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id, format='raw').execute()
        message = message['snippet']
        # msg_raw = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
        # msg_str = email.message_from_bytes(msg_raw).get_payload(0)
        # print(message)
        # root = tkinter.Tk()
        # root.title("MailGuard")
        # root.geometry("600x600")
        # mail = Text(root)
        # mail.insert(INSERT, msg_str)
        # mail.pack()
        # root.mainloop()
        return message
    except(errors.HttpError, error):
        print("An error occurred !!!")
